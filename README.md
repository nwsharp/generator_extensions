# generator_extensions
`generator_extensions` provides a set of blanket-implemented traits which
provide parity between Generators and Iterators.

The two extension traits provided by `generator_extensions` are `Resumable`,
which represents a resumable reference to a `Generator`, and `IntoGenerator`
which represents a type which can be converted into a `Generator`.

The `Resumable` trait provides extension methods to unify pinned and `Unpin`
generators, transform the outputs of generators, and convert certain types of
generators into Iterators.

The `IntoGenerator` trait permits conversion of `Iterator`s into `Generator`s.
Due to the way built-in generator types are expressed by the Rust compiler, the
`Gen` newtype wrapper is provided to allow compiler-generated and external
generator types to interoperate with `IntoGenerator`.

# Documentation
Inline rustdoc documentation is available. A mirror of this documentation is
available at <https://docs.rs/generator_extensions>.

# Contributing
`generator_extensions` is developed at [GitLab].

Reasonable performance, correctness, documentation, and ease-of-use
contributions are always welcome. [Bug reports][Issues] and feature suggestions
are accepted through GitLab.

## Pull Requests
Please ensure pull requests match the existing coding style and are formatted
with rustfmt.

Spelling and grammatical errors are considered bugs, so please use
spell-checking facilities prior to submitting a contribution.

## Contribution Agreement
By contributing, you grant all contributors a perpetual, worldwide,
non-exclusive, no-charge, royalty-free, irrevocable copyright license to
reproduce, prepare derivative works of, publicly display, publicly perform,
relicense, sublicense, and distribute your contributions.

Additionally, you affirm that you are legally entitled to grant such license and
that your contributions are not and will not become patent-encumbered. In the
event that you discover that such affirmation was made in error, you agree to
post notice of such error in a conspicuous place (such as a
[GitLab Issue][Issues]) within three days.

# License
`generator_extensions` is licensed under the terms of the
[Apache License, Version 2.0][Apache2] or the [MIT License][MIT].

The corresponding [SPDX] license identifier is `Apache-2.0 OR MIT`.

# Copyright
This document is Copyright (C) 2020 Nathan Sharp.

Permission is granted to reproduce this document, in any form, free of charge.

[Apache2]: https://www.apache.org/licenses/LICENSE-2.0
[GitLab]: https://gitlab.com/nwsharp/generator_extensions
[Issues]: https://gitlab.com/nwsharp/generator_extensions/-/issues
[MIT]: https://opensource.org/licenses/MIT
[SPDX]: https://spdx.org/licenses
