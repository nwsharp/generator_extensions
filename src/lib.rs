//
// Copyright (C) 2020 Nathan Sharp.
//
// This file is available under either the terms of the Apache License, Version
// 2.0 or the MIT License, at your discretion.
//

#![no_std]
#![feature(generator_trait)]

//! `generator_extensions` provides a set of blanket-implemented traits which
//! provide parity between [`Generator`]s and [`Iterator`]s.
//!
//! To take advantage of the blanket implementations, simply write the
//! following, which imports all of the extension traits into the current scope:
//! ```
//! use generator_extensions::prelude::*;
//! ```
//!
//! The two extension traits provided by `generator_extensions` are
//! [`Resumable`], which represents a resumable reference to a [`Generator`],
//! and [`IntoGenerator`] which represents a type which can be converted into a
//! [`Generator`].
//!
//! The [`Resumable`] trait provides extension methods to unify [Resumable] and
//! [`Unpin`] generators, transform the outputs of generators, and convert
//! certain types of generators into [`Iterator`]s.
//!
//! The [`IntoGenerator`] trait permits conversion of [`Iterator`]s into
//! [`Generator`]s. Due to the way built-in generator types are expressed by the
//! Rust compiler, the [`Gen`] newtype wrapper is provided to allow
//! compiler-generated and external generator types to interoperate with
//! [`IntoGenerator`].
//!
//! # License
//! `generator_extensions` is licensed under the terms of the
//! [Apache License, Version 2.0][Apache2] or the [MIT License][MIT].
//!
//! # Development
//! `generator_extensions` is developed at [GitLab].
//!
//! [Apache2]: https://www.apache.org/licenses/LICENSE-2.0
//! [`Generator`]: core::ops::Generator
//! [GitLab]: https://gitlab.com/nwsharp/generator_extensions
//! [`IntoGenerator`]: iter::IntoGenerator
//! [`Iterator`]: core::iter::Iterator
//! [MIT]: https://opensource.org/licenses/MIT
//! [Resumable]: core::pin::Pin
//! [`Resumable`]: gen::Resumable
//! [`Unpin`]: core::marker::Unpin

mod gen;
mod iter;

#[cfg(test)]
mod tests;

pub use gen::*;
pub use iter::*;

// Re-export the core generator types in case they move.
pub use core::ops::{Generator, GeneratorState};

/// The generator_extensions prelude.
///
/// To easily make use of the blanket traits provided by generator_extensions,
/// simply write:
/// ```
/// use generator_extensions::prelude::*;
/// ```
pub mod prelude {
    pub use crate::gen::Resumable;
    pub use crate::iter::IntoGenerator;
}

use core::marker::PhantomData;

struct NoData<T: ?Sized> {
    _phantom: PhantomData<*const T>,
}

// Safety: This type permits no operations aside from move and drop.
unsafe impl<T: ?Sized> Send for NoData<T> {}
unsafe impl<T: ?Sized> Sync for NoData<T> {}

impl<T: ?Sized> NoData<T> {
    #[must_use]
    pub fn new() -> Self {
        Self { _phantom: PhantomData }
    }
}
